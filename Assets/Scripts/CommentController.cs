﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CommentController : MonoBehaviour {

	public Text commentPrefab;
	public ScrollRect scrollRect;
	public VerticalLayoutGroup layoutGroup;
	public GameObject[] stars;

	public string[] names = new string[]{"Liam", "Etienne", "Luke", "Ash", "Ben", "Connor", "Troy", "Harry", 
	                                 "Ryan", "Dineth", "Joel", "Rohan", "Shane", "Dan", "Adrian", "Shaun",
	                                 "Gary", "Tony", "Steve", "Chloe", "Sophia", "Hannah", "Brittany", 
	                                 "Emily", "Elizabeth", "Sarah", "Megan", "Ellen", "Lucy", "Rachel",
									 "Allison", "Sophie", "Nicole", "xXSlayerXx" , "John Cena"};

	public string[] positiveComment = new string[]{"Inspired!", "mmmm Delicious", "Excellent Choice!", "MORE CHEESE!!",
	                                             "Great idea", "can't wait to make my own", "I never would have thought of thaat",
												 ":)"};

	public string[] negativeComment = new string[]{"ummm are you sure?", "i guess that.... could work?", "What does your sandwich have feelings or something?",
	                                             "mmm DELICIOUS!", "YUCK!", "why am I even watching this happen?", 
											   	 "oh.. I'mm uhhh... alergic"};


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {




	
	}

	public void AddComment(bool goodIngredient)
	{
		Text newComment = Instantiate (commentPrefab);

		if (goodIngredient) {
			newComment.text = names [Random.Range (0, names.Length)] + ": \n" 
				+ positiveComment [Random.Range (0, positiveComment.Length)];
		} else {
			newComment.text = names [Random.Range (0, names.Length)] + ": \n" 
				+ negativeComment [Random.Range (0, negativeComment.Length)];
		}


		newComment.transform.SetParent (layoutGroup.transform, false);
		StartCoroutine (ScrollDown ());

	}

	public void RateSandwich(int numStars)
	{
		for (int i = 0; i < numStars; i++) {
			stars [i].SetActive (true);
		}

	}
	public IEnumerator ScrollDown()
	{
		yield return new WaitForSeconds (0.05f);
		scrollRect.verticalNormalizedPosition = 0.0f;
	}


}
