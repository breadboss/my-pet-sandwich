﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public string[] audioName;//List Name of the audio
    public AudioClip[] audioClip;//List of audioclip
    public bool clipFound;//Check if the audio is working

    //Play the audio when needed
    public void Play(string clipName)
    {
        //Check throught the list if the audio is there
        for (int i = 0; i < audioName.Length; i++)
        {
            if (clipName == audioName[i])//the audio is there, play it
            {
                GetComponent<AudioSource>().clip = audioClip[i];
                GetComponent<AudioSource>().Play();
                clipFound = true;
                break;
            }
            else//The audio is not here
            { 
               clipFound = false;
                }
		}
		//Show the audio was not played
		if (!clipFound) {
			Debug.Log("Audio not found");
		}

        //I need to find a way to access the sound input more easily
       
        //If you want to play the sound in the script, use the line bellow to play it;
        //To note, you need to have condition to make it work, or else it will spawn the sound infinitly
        //GameObject.Find("AudioManager").SendMessage("Play", "Sound1"); - After play, use the name of the sound
    }
}
