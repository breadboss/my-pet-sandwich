﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlayerInteractionController : MonoBehaviour {

	public NavMeshAgent sandwichAgent;
	public GameObject sandwich;
    public GameObject knife;
	public GameObject currentGrabedObject;
	public GameObject lettice;
	public GameObject tomato;
	public GameObject cheese;
	public GameObject swiss;
	public GameObject ham;
	public GameObject chichen;
	public GameObject onion;
	public GameObject moldycheddar;
	public GameObject dogTreat;
	public GameObject brokenGlass;

	public Vector3 mousePosition;
	public float mouseMovementEnter;
	public Vector3 objectPos;
	public int hitLayerNum;
	public GameObject cursor;
	public float distBetweenObjects;
	public float moveAlongZ;
	public float moveAlongZEnter;
	public List<GameObject> thrownFood = new List<GameObject>();

	//used to lerp the sandwich at the end of the game so it can be eaten
	public Transform sandwichStartPoint;
	public Transform sandwichEndPoint;
	public Vector3 setSandwichEndPoint;
	public float sandwichSpeed; 
	public float sandwichstartLerpTime;
	public float sandwichJourneyLength; 
	public float sandwichDistanceCovered;
	public float sandwichJourney;
	public bool sandwichJourneyStart;
	public bool sandwichJourneyEnd;

	public Vector3 grabbedOffset;
	public Vector3 startObjectPos;
	public Vector3 endObjectPos;

	public bool isMoving;
	public Vector3 mouseTrack;

	public Sandwich sandwichScript;

	void Start () 
	{
		cursor = GameObject.Find ("Cursor").gameObject;
		sandwichEndPoint.position = setSandwichEndPoint;
		mouseTrack = Input.mousePosition;
	}

	void Update () 
	{
		RayCastMouse ();
		FigureOutMousePosition ();
		Sandwich sandwichScript = GetComponent<Sandwich> ();
		CheckEnd ();
	}

	void RayCastMouse ()
	{
		if (!PauseController.isPaused) {
			if (Input.GetMouseButtonDown (0)) {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit)) {
					hitLayerNum = hit.transform.gameObject.layer;
					switch (hitLayerNum) {
					case 8:
						Debug.Log ("hit Lettice");
						currentGrabedObject = Instantiate (lettice, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 9:
						Debug.Log ("hit Tomato");
						currentGrabedObject = Instantiate (tomato, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 10:
						Debug.Log ("hit Cheese");
						currentGrabedObject = Instantiate (cheese, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 11:
						Debug.Log ("hit Ham");
						currentGrabedObject = Instantiate (ham, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 12:
						Debug.Log ("hit Chicken");
						currentGrabedObject = Instantiate (chichen, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 14:
						Debug.Log ("hit Onion");
						currentGrabedObject = Instantiate (onion, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 16:
						Debug.Log ("hit brokenGlass");
						currentGrabedObject = Instantiate (brokenGlass, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 17:
						Debug.Log ("hit moldycheddar");
						currentGrabedObject = Instantiate (moldycheddar, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 18:
						Debug.Log ("hit dogTreat");
						currentGrabedObject = Instantiate (dogTreat, hit.point, transform.rotation) as GameObject;
						Grabbed();
						break;
					case 15:
						sandwichAgent.SetDestination (hit.transform.position);
						break;
					case 25:
						Debug.Log ("hit Knife");
                        currentGrabedObject = Instantiate(knife, hit.point, transform.rotation) as GameObject;
                        Grabbed();
                        //currentGrabedObject = knife;
                        //Grabbed();
                        break;
					case 24:
						Debug.Log ("Hit Sandwich");
						if (sandwichScript.currentSize >= sandwichScript.maxSize) {
							Debug.Log ("EndGame");
							GameObject.Find ("GameEnds").GetComponent<GameEndTrigger> ().gameEnd = true;
							//Application.LoadLevel("Title");
						}
						break;
					}
				} 
			}
			if (Input.GetMouseButtonUp (0) && currentGrabedObject != null) {
				if (currentGrabedObject.transform.parent == cursor.transform) {
					currentGrabedObject.transform.parent = null;
					Cursor.visible = true;
				}
				Rigidbody currentGrabedObjectRigidbody;
				currentGrabedObjectRigidbody = currentGrabedObject.GetComponent<Rigidbody> ();
				currentGrabedObjectRigidbody.useGravity = true;
				if (isMoving)
				{
				    currentGrabedObjectRigidbody.AddForce (endObjectPos - startObjectPos);
				}
				thrownFood.Add (currentGrabedObject);

			}
			if (Input.GetMouseButtonDown (1)) {
				if (currentGrabedObject != null) {
					Destroy (currentGrabedObject.gameObject);
					Cursor.visible = true;
				}
			}
		}
	}

	void CheckEnd ()
	{
		if (sandwichJourneyStart) 
		{
			sandwichDistanceCovered = (Time.time - sandwichstartLerpTime) * sandwichSpeed;
			sandwichJourneyLength = Vector3.Distance (sandwichStartPoint.position, sandwichEndPoint.position);		
			sandwichJourney = sandwichDistanceCovered/sandwichJourneyLength;
			sandwich.transform.position = Vector3.Lerp(sandwichStartPoint.position, sandwichEndPoint.position, sandwichJourney);
		}
	}

	void FigureOutMousePosition ()
	{
		mousePosition = Input.mousePosition;
		cursor.transform.position = mousePosition;
		mousePosition.z = cursor.transform.position.z - Camera.main.transform.position.z;
		cursor.transform.position = Camera.main.ScreenToWorldPoint (mousePosition); 
		endObjectPos = mousePosition;
		if (mouseTrack != Input.mousePosition) {
			isMoving = true;
			mouseTrack = Input.mousePosition;
		} else {
			isMoving = false;
			startObjectPos = mousePosition;
		} 
	}

	void Grabbed()
	{
		startObjectPos = mousePosition;
		currentGrabedObject.transform.rotation = Quaternion.Euler (-90, 0, 0);
		currentGrabedObject.transform.parent = cursor.transform;
		Cursor.visible = false;
	}
}
