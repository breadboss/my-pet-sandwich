﻿using UnityEngine;
using System.Collections;

public class GrillController : MonoBehaviour {

	public Sandwich sandwichScript;
	public Recipe recipe;
	public GameObject sandwich;
	public GameObject sandwichTop;
	public GameObject sandwichBottom;
	public bool onGrill;
	public bool completelyBurnt;
	public int maxGrillTimer;
	public float GrillTimerSum;
	public float resetTimer;
	public Material grillState1;
	public Material grillState2;
	public Material grillState3;
	public Material grillState4;
	public Renderer rendTop;
	public Renderer rendBottom;

    public AudioSource toasting;//To make the grilling sound

	public ParticleSystem particleSystemFire;

	void Start () 
	{
		onGrill = false;
		GrillTimerSum = resetTimer;
		particleSystemFire.enableEmission = false;
	}

	void Update () 
	{
		CheckGrillOn ();
		//CheckGrillOff ();
		CheckGrillStates ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == sandwich) 
		{
			onGrill = true;
			rendTop = sandwichTop.gameObject.GetComponent<Renderer> ();
			rendBottom = sandwichBottom.gameObject.GetComponent<Renderer> ();
			particleSystemFire.enableEmission = true;
            //Play sound of toasting
            toasting.enabled = true;
            toasting.loop = true;

            if (sandwichScript.sandwichState == 7)
			{
				Debug.Log ("working");
				recipe.AddIngredient("Toasted", true);
                sandwichScript.currentSize++;
			}
			else if(sandwichScript.sandwichState != 7){
				Debug.Log (" not working");
				recipe.AddIngredient("Toasted", false);
			}
        }
	}

	void OnTriggerExit (Collider other)
	{
		if (other.gameObject == sandwich) 
		{
			onGrill = false;
            //Stop playing the sound
            toasting.enabled = false;
            toasting.loop = false;
			particleSystemFire.enableEmission = false;
        }
	}

	void CheckGrillOn ()
	{
		if (onGrill) 
		{
			GrillTimerSum -= Time.deltaTime;
			maxGrillTimer = Mathf.FloorToInt(GrillTimerSum);
		}
	}

	/*void CheckGrillOff ()
	{
		if (!onGrill) 
		{
			GrillTimerSum = resetTimer;
		}
	} */

	void CheckGrillStates ()
	{

		switch (maxGrillTimer)
		{
		case 15:
			Debug.Log ("Hit 1 State");
			rendTop.sharedMaterial = grillState1;
			rendBottom.sharedMaterial = grillState1;
			break;
		case 10:
			Debug.Log ("Hit 2 State");
			rendTop.sharedMaterial = grillState2;
			rendBottom.sharedMaterial = grillState2;
			break;
		case 5:
			Debug.Log ("Hit 3 State");
			rendTop.sharedMaterial = grillState3;
			rendBottom.sharedMaterial = grillState3;
			break;
		case 0:
			Debug.Log ("Hit 4 State");
			rendTop.sharedMaterial = grillState4;
			rendBottom.sharedMaterial = grillState4;
			break;
		}
	}
}
