﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CustomizerController : MonoBehaviour {

	public Camera gameCamera;

	public Canvas sandwichCreationCanvas; 
	public GameObject sandwich;

//	public Button eyeType1Button;
//	public Button eyeType2Button;
//	public Button eyeType3Button;
//	public Button eyeType4Button;
//	public Button eyeType5Button;
//	public Button eyeType6Button;
//	public Button eyeType7Button;
//	public Button eyeType8Button;
//	public Button eyeType9Button;
//	public Button eyeType10Button;
//	public Button eyeType11Button;
//	public Button eyeType12Button;
//	public Button eyeType13Button;
//	public Button eyeType14Button;
	public Sprite eyeType1;
	public Sprite eyeType2;
	public Sprite eyeType3;
	public Sprite eyeType4;
	public Sprite eyeType5;
	public Sprite eyeType6;
	public Sprite eyeType7;
	public Sprite eyeType8;
	public Sprite eyeType9;
	public Sprite eyeType10;
	public Sprite eyeType11;
	public Sprite eyeType12;
	public Sprite eyeType13;
	public Sprite eyeType14;

	public GameObject hat0;
	public GameObject hat1;
	public GameObject hat2;
	public GameObject hat3;
	public GameObject hat4;
	public GameObject hat5;
	public GameObject hat6;

	public Image eyeSpawner1;
	public Image eyeSpawner2;
    public Image eyeSpawnerEnding;
	public int eyeType;
	public Button breadType1Button;
	public Button breadType2Button;
	public Button breadType3Button;
	public Button breadType4Button;
	public Material breadType1;
	public Material breadType2;
	public Material breadType3;
	public Material breadType4;
	public Renderer rendTop;
	public Renderer rendBottom;
	public int breadType;
	public string sandwichName;
	public InputField inputName;
	public bool isNamed;
	public bool hasEyes;
	public bool hasBread;
	public bool isComplete;
	public Transform startPoint;
	public Transform endPoint;
	public Vector3 setEndPoint;
	public float camSpeed; 
	public float startLerpTime;
	public float journeyLength; 
	public float distanceCovered;
	public float journey;
	public bool journeyEnd;
	public AnimationCurve speedCurve;

	public Animation animUI;
	public GameObject rightPanel;
	public GameObject leftPanel;
	public Button playButton;
	public Button randButton;
	public GameObject nameInput;
	public GameObject hatPanel;
	public GameObject eyePanel;


	void Start ()
	{
		startPoint.position = gameCamera.transform.position;
		endPoint.position = setEndPoint;
		hasEyes = true;
		hasBread = true;
		rightPanel.GetComponent<Animation>().Play();
		leftPanel.GetComponent<Animation>().Play();
		playButton.GetComponent<Animation>().Play();
		randButton.GetComponent<Animation>().Play();
		nameInput.GetComponent<Animation>().Play();
		hatPanel.GetComponent<Animation>().Play();
		eyePanel.GetComponent<Animation>().Play();
	}

	void Update () 
	{
		IsComplete ();
		InputName ();
		//EyePos ();
	}

	void InputName()
	{
		if (inputName.text.Length >= 1) 
		{
			sandwichName = inputName.text;
			PlayerPrefs.SetString("SandwichName",sandwichName);
			isNamed = true;
		}
	}

	void IsComplete()
	{
		if (isComplete == true) 
		{
			distanceCovered = (Time.time - startLerpTime) * camSpeed;
			journeyLength = Vector3.Distance (startPoint.position, endPoint.position);		
			journey = distanceCovered/journeyLength;
			gameCamera.transform.position = Vector3.Lerp(startPoint.position, endPoint.position,speedCurve.Evaluate(journey));
		}
	}

	public void HatType0(){
		Debug.Log ("Hat 1");
		//GameObject.Find ("Hat1").SetActive (true);
		hat0.SetActive (true);
		hat1.SetActive (false);
		hat2.SetActive (false);
		hat3.SetActive (false);
		hat4.SetActive (false);
		hat5.SetActive (false);
		hat6.SetActive (false);
	}

	public void HatType1(){
		Debug.Log ("Hat 1");
		//GameObject.Find ("Hat1").SetActive (true);
		hat0.SetActive (false);
		hat1.SetActive (true);
		hat2.SetActive (false);
		hat3.SetActive (false);
		hat4.SetActive (false);
		hat5.SetActive (false);
		hat6.SetActive (false);
	}

	public void HatType2(){
		Debug.Log ("Hat 2");
		//GameObject.Find ("Hat1").SetActive (true);
		hat0.SetActive (false);
		hat1.SetActive (false);
		hat2.SetActive (true);
		hat3.SetActive (false);
		hat4.SetActive (false);
		hat5.SetActive (false);
		hat6.SetActive (false);
	}

	public void HatType3(){
		Debug.Log ("Hat 3");
		//GameObject.Find ("Hat1").SetActive (true);
		hat0.SetActive (false);
		hat1.SetActive (false);
		hat2.SetActive (false);
		hat3.SetActive (true);
		hat4.SetActive (false);
		hat5.SetActive (false);
		hat6.SetActive (false);
	}

	public void HatType4(){
		Debug.Log ("Hat 4");
		//GameObject.Find ("Hat1").SetActive (true);
		hat0.SetActive (false);
		hat1.SetActive (false);
		hat2.SetActive (false);
		hat3.SetActive (false);
		hat4.SetActive (true);
		hat5.SetActive (false);
		hat6.SetActive (false);
	}

	public void HatType5(){
		Debug.Log ("Hat 5");
		//GameObject.Find ("Hat1").SetActive (true);
		hat0.SetActive (false);
		hat1.SetActive (false);
		hat2.SetActive (false);
		hat3.SetActive (false);
		hat4.SetActive (false);
		hat5.SetActive (true);
		hat6.SetActive (false);
	}

	public void HatType6(){
		Debug.Log ("Hat 6");
		//GameObject.Find ("Hat1").SetActive (true);
		hat0.SetActive (false);
		hat1.SetActive (false);
		hat2.SetActive (false);
		hat3.SetActive (false);
		hat4.SetActive (false);
		hat5.SetActive (false);
		hat6.SetActive (true);
	}


	public void EyeType1()
	{
		eyeSpawner1.sprite = eyeType1;
        eyeSpawnerEnding.sprite = eyeType1;
        //eyeSpawner2.sprite = eyeType1;

        eyeType = 1;
		hasEyes = true;
	}

	public void EyeType2()
	{
		eyeSpawner1.sprite = eyeType2;
        eyeSpawnerEnding.sprite = eyeType2;
        //eyeSpawner2.sprite = eyeType2;

        eyeType = 2;
		hasEyes = true;
	}

	public void EyeType3()
	{
		eyeSpawner1.sprite = eyeType3;
        eyeSpawnerEnding.sprite = eyeType3;
        //eyeSpawner2.sprite = eyeType3;

        eyeType = 3;
		hasEyes = true;;
	}

	public void EyeType4()
	{
		eyeSpawner1.sprite = eyeType4;
        eyeSpawnerEnding.sprite = eyeType4;
        //eyeSpawner2.sprite = eyeType4;

        eyeType = 4;
		hasEyes = true;
	}

	public void EyeType5()
	{
		eyeSpawner1.sprite = eyeType5;
        eyeSpawnerEnding.sprite = eyeType5;
        //eyeSpawner2.sprite = eyeType5;

        eyeType = 5;
		hasEyes = true;
	}

	public void EyeType6()
	{
		eyeSpawner1.sprite = eyeType6;
        eyeSpawnerEnding.sprite = eyeType6;
        //eyeSpawner2.sprite = eyeType6;

        eyeType = 6;
		hasEyes = true;
	}

	public void EyeType7()
	{
		eyeSpawner1.sprite = eyeType7;
        eyeSpawnerEnding.sprite = eyeType7;
        //eyeSpawner2.sprite = eyeType7;

        eyeType = 7;
		hasEyes = true;
	}

	public void EyeType8()
	{
		eyeSpawner1.sprite = eyeType8;
        eyeSpawnerEnding.sprite = eyeType8;
        //eyeSpawner2.sprite = eyeType8;

        eyeType = 8;
		hasEyes = true;
	}

	public void EyeType9()
	{
		eyeSpawner1.sprite = eyeType9;
		//eyeSpawner2.sprite = eyeType8;
		
		eyeType = 9;
		hasEyes = true;
	}

	public void EyeType10()
	{
		eyeSpawner1.sprite = eyeType10;
		//eyeSpawner2.sprite = eyeType8;
		
		eyeType = 10;
		hasEyes = true;
	}

	public void EyeType11()
	{
		eyeSpawner1.sprite = eyeType11;
		//eyeSpawner2.sprite = eyeType8;
		
		eyeType = 11;
		hasEyes = true;
	}

	public void EyeType12()
	{
		eyeSpawner1.sprite = eyeType12;
		//eyeSpawner2.sprite = eyeType8;
		
		eyeType = 12;
		hasEyes = true;
	}

	public void EyeType13()
	{
		eyeSpawner1.sprite = eyeType13;
		//eyeSpawner2.sprite = eyeType8;
		
		eyeType = 13;
		hasEyes = true;
	}

	public void EyeType14()
	{
		eyeSpawner1.sprite = eyeType14;
		//eyeSpawner2.sprite = eyeType8;
		
		eyeType = 14;
		hasEyes = true;
	}
	public void BreadType1()
	{
		rendTop.sharedMaterial = breadType1;
		rendBottom.sharedMaterial = breadType1;
		breadType = 1;
		hasBread = true;
	}

	public void BreadType2()
	{
		rendTop.sharedMaterial = breadType2;
		rendBottom.sharedMaterial = breadType2;
		breadType = 2;
		hasBread = true;
	}

	public void BreadType3()
	{
		rendTop.sharedMaterial = breadType3;
		rendBottom.sharedMaterial = breadType3;
		breadType = 3;
		hasBread = true;
	}

	public void BreadType4()
	{
		rendTop.sharedMaterial = breadType4;
		rendBottom.sharedMaterial = breadType4;
		breadType = 4;
		hasBread = true;
	}

	public void PlayButton()
	{
		if (isNamed && hasEyes && hasBread) 
		{
			isComplete = true;
			startLerpTime = Time.time;
			//sandwichCreationCanvas.enabled = false;

			rightPanel.GetComponent<Animation>().Play("tweeningAnimation");
			leftPanel.GetComponent<Animation>().Play("tweenAnimationCustomiseButtons");
			playButton.GetComponent<Animation>().Play("tweenAnimationPlay");
			randButton.GetComponent<Animation>().Play("tweenAnimationRandom");
			nameInput.GetComponent<Animation>().Play("tweeningAnimationName");
			hatPanel.GetComponent<Animation>().Play("tweeningAnimation");
			eyePanel.GetComponent<Animation>().Play("tweeningAnimation");

            Application.LoadLevelAdditive("Instructions");
		}
	}



	//void EyePos()
	//{
	//	//eyeSpawned1.transform.position = eyeSpawner1.transform.position;
	//	//eyeSpawned2.transform.position = eyeSpawner2.transform.position;
	//	eyeSpawned1.transform.parent = eyeSpawner1;
	//	eyeSpawned2.transform.parent = eyeSpawner2;
	//}
}
