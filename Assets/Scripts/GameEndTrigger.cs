﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
public class GameEndTrigger : MonoBehaviour {

    //In this script, it activate via the Sandwichscript
    //Objective of the script, a lot of variable can be easily change from the Unity Inspector Under GameEnds
    //Need to bring the sandwich to the camera
    //Black Screen
    //Can see eyes at the end
	//FAdeABlackScreenForTheEnd
    //Return to the main menu

    public string scene;
    public float timerOfBlackScreen;//Duration of the blackScreen
    public float timerBeforeEndgame;//Duration until the game ends
	public bool gameEnd = false;//Check if the game is end
    public bool blackScreen = true;//To make the blackscreen only spawn once
	public bool fadingBlackScreen = true;//To make the blackScreenfading only spawn once

    public GameObject eyesEnding;//Game Object with the eyes the player choose at the start
    public Transform placeAtEnding;//Place where the eyes need to be at the end

    void Update () {

		if (gameEnd)
		{
            BlackScreen();//Load Black Screen
            timerOfBlackScreen -= Time.deltaTime;//Reduce timer

            Destroy(GameObject.Find("SandwichAgent"));//Destroy the sandwich
            eyesEnding.transform.position = placeAtEnding.transform.position;//Place the eyes

            if (timerOfBlackScreen <= 0)//Destroy black screen
            {
                Debug.Log("DestroyBlackScreen");
                Destroy(GameObject.Find("BlackScreen"));
                timerBeforeEndgame -= Time.deltaTime;
            }

			if (timerOfBlackScreen <= -3) {
				FadingBlackScreenEnding();//Make the fade in
			}

            if (timerBeforeEndgame <= 0)//End the game after the blackScreen
            {
                Application.LoadLevel("Title");
            }
        }
	}

    public void BlackScreen()
    {
        if (blackScreen)
        {
            Application.LoadLevelAdditive("BlackScreen");
            blackScreen = false;//Make the black screen only load once
			GameObject.Find("AudioManager").SendMessage("Play", "EndingSoundProper");
        }
    }

	public void FadingBlackScreenEnding() {

		if (fadingBlackScreen) {
		
			Application.LoadLevelAdditive ("BlackScreenFadingEnd");
			fadingBlackScreen = false;//Make the black screen only load once
		}
	}
}
