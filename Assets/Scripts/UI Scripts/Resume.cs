﻿using UnityEngine;
using System.Collections;

public class Resume : MonoBehaviour {
	public PauseController player;
	//Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<PauseController> ();
	}
	public void OnClick()
	{
		player.Resume ();
	}
}