﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RandomizeCustomisationButton : MonoBehaviour {

    //This script is use for the random customization of the sandwich

    public string[] eyeName;//List of Name of the eye's image
    public Sprite[] eyeImage;//List of Images of the eye's
    public string[] breadNameTexture;//List of Name of the breadTexture
    public Material[] breadTexture;//List of the material of the bread

    public int randomDice1;//Random value for eyes
    public int randomDice2;//Random value for breadtexture

	public int randomDice3;//Random value for hats
    public CustomizerController customizerController;

    public Image eyeSpawner;//Component to search the eye spawner
    public int eyeType;//Component to search the eyeType number

    public Renderer rendTop;//Component to search the top bread material
    public Renderer rendBottom;//Component to search the bottom bread material
    public int breadType;//Component to search the breadtype number

	//public GameObject spawnHat;//Spawm the hat game object

    public Image eyeSpawnerEnding;//For the eyes at the end of the game

    //Search the component needed to add eyes or textures
    public void Awake()
    {
        eyeSpawner = GameObject.Find("Main Camera").GetComponent<CustomizerController>().eyeSpawner1;
        eyeType = GameObject.Find("Main Camera").GetComponent<CustomizerController>().eyeType;
        rendTop = GameObject.Find("Main Camera").GetComponent<CustomizerController>().rendTop;
        rendBottom = GameObject.Find("Main Camera").GetComponent<CustomizerController>().rendBottom;
        breadType = GameObject.Find("Main Camera").GetComponent<CustomizerController>().breadType;
        customizerController = GameObject.Find("Main Camera").GetComponent<CustomizerController>();
    }

    //Customization button code
    public void Customize()
    {
        //Generate a random number in the list of Eye types
        randomDice1 = Random.Range(0, eyeName.Length);

        eyeSpawner.sprite = eyeImage[randomDice1];
        eyeSpawnerEnding.sprite = eyeImage[randomDice1];
        eyeType = randomDice1;

        //Generate a random number in the list of bread texture type
        randomDice2 = Random.Range(0, breadNameTexture.Length);

        rendTop.sharedMaterial = breadTexture[randomDice2];
        rendBottom.sharedMaterial = breadTexture[randomDice2];
        breadType = randomDice2;

		//Generate a random number in the list of hats
		randomDice3 = Random.Range (0, 12);

        if (randomDice3 <= 5)
        {
            customizerController.HatType0();
        }

        if (randomDice3 == 6)
        {
            customizerController.HatType1();
        }

        if (randomDice3 == 7)
        {
            customizerController.HatType2();
        }

        if (randomDice3 == 8)
        {
            customizerController.HatType3();
        }

        if (randomDice3 == 9)
        {
            customizerController.HatType4();
        }

        if (randomDice3 == 10)
        {
            customizerController.HatType5();
        }

        if (randomDice3 == 11)
        {
            customizerController.HatType6();
        }
    }

    
}
