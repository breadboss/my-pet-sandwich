﻿using UnityEngine;
using System.Collections;
public class LoadSceneButton : MonoBehaviour {
	public string scene;
	public void OnClick()
	{
		Application.LoadLevel (scene);
		Time.timeScale = 1;
	}

    public void LeaveGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
