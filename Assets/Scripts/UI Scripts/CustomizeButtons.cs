﻿using UnityEngine;
using System.Collections;

public class CustomizeButtons : MonoBehaviour {
	public GameObject eyeTypeCanvas;
	public GameObject breadTypeCanvas;
	public GameObject hatTypeCanvas;
	// Use this for initialization
	public void OnClickEye () {
		eyeTypeCanvas.SetActive (true);
		breadTypeCanvas.SetActive (false);
		hatTypeCanvas.SetActive (false);
	}
	public void OnClickBread () {
		eyeTypeCanvas.SetActive (false);
		breadTypeCanvas.SetActive (true);
		hatTypeCanvas.SetActive (false);
	}
	public void OnClickHat () {
		eyeTypeCanvas.SetActive (false);
		breadTypeCanvas.SetActive (false);
		hatTypeCanvas.SetActive (true);
	}
	// Update is called once per frame
	void Update () {
	
	}
}
