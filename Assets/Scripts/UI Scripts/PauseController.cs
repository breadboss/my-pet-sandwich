﻿using UnityEngine;
using System.Collections;
public class PauseController : MonoBehaviour {
	public string pauseMenu;
	public GameObject pauseMenuCanvas;
	public static bool isPaused = false;

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape) && isPaused == false) 
		{
			Pause ();
		}
		//if game is already pause, then resume
		else if (Input.GetKeyUp(KeyCode.Escape) && isPaused == true)
		{
			Resume();
		}

	}
	//If you spawn the pause button when eating, the sound will restart again from the start, but normally it is fine
	//runs pause sequence if you press escape and if it isnt already paused
	public void Pause()
	{
		Application.LoadLevelAdditive(pauseMenu);
		Time.timeScale = 0.0f;
		isPaused = true;
		GameObject.Find ("AudioManager").GetComponent<AudioSource> ().Pause();//Pause audio during the pause menu
		GameObject.Find ("Sandwich").GetComponent<AudioSource> ().Pause();
		GameObject.Find ("GrillBottom").GetComponent<AudioSource> ().Pause();
	}
	//resumes the game
	public void Resume()
	{
		pauseMenuCanvas = GameObject.FindGameObjectWithTag("PauseMenuCanvas");
		Destroy(pauseMenuCanvas);
		Time.timeScale = 1;
		isPaused = false;
		GameObject.Find ("AudioManager").GetComponent<AudioSource> ().Play();//Restart audio during after the pause menu
		GameObject.Find ("Sandwich").GetComponent<AudioSource> ().Play();
		GameObject.Find ("GrillBottom").GetComponent<AudioSource> ().Play();

	}
}
