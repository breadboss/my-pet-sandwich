﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Sandwich : MonoBehaviour {

	public PlayerInteractionController playerController;
	public GrillController grillController;
	public Recipe recipe;
	public CommentController comments;
	public NavMeshAgent sandwichNav;
	public Animation anim;
	public int nextFood;
	public Vector3 targetFood;
	public GameObject endTarget;

	public GameObject sandwichTop;
	public GameObject sandwichBottom;
	public float breadHeight;
	public int maxSize;
	public int currentSize;
	public int rating = 0;

	// Sandwich States
	public int sandwichState;
	private const int idle = 0;
	private const int tomato = 1;
	private const int lettuce = 2;
	private const int cheese = 3;
	private const int ham = 4;
	private const int chicken = 5;
	private const int onion = 6;
	private const int toasty = 7;
	private const int moldy = 8;
	private const int dogfood = 9;
	private const int glass = 10;
	private const int knife = 11;
	private const int cutMe = 12;

	public GameObject thoughtBubble;
	public Image sandwichWants;
	public Sprite tomatoSprite;
	public Sprite lettuceSprite;
	public Sprite cheeseSprite;
	public Sprite hamSprite;
	public Sprite chickenSprite;
	public Sprite onionSprite;
	public Sprite toastingSprite;
	public Sprite moldySprite;
	public Sprite dogFoodSprite;
	public Sprite glassSprite;
	public Sprite knifeSprite;
	public Sprite cuttingSprite;


	public bool fed = false;
	public bool willEat = true;
	public bool matching = false;
	public float wantTimer = 0;

	public ParticleSystem particleSystemEmotion;
	public Renderer particleSystemRenderer;
	public Material sadState;
	public Material happyState;
	public float emissionTimer;
	public float emissionTimerReset;
	public bool emissionStart;

	public ParticleSystem cryingParticleRight;
	public ParticleSystem cryingParticleLeft;
	public int cryLimit;
	public int cryLimitSet;

    public int happyCounter;

	private Animator myAnimator;

	//public Animation animation;

	// Use this for initialization
	void Start () {
		sandwichState = 0;
		breadHeight = sandwichTop.transform.localPosition.y;
		particleSystemEmotion.enableEmission = false;
		particleSystemRenderer = particleSystemEmotion.GetComponent <Renderer> ();
		cryingParticleRight.enableEmission = false;
		cryingParticleLeft.enableEmission = false;
		myAnimator = GetComponent<Animator>();
//		Debug.Log (breadHeight);
	
	}
	
	// Update is called once per frame
	void Update () {
		if (emissionStart) 
		{
			checkEmission ();
		}
		CryCheck ();
		switch(sandwichState) {
		case idle:
			myAnimator.SetBool("isMoving", false);
			//anim.Play ("Idle");
			//anim.Play ("Idle");
			sandwichWants.sprite = null;
		
			thoughtBubble.SetActive(false);
			SandwichBehaviour();
			break;
		case tomato:
			sandwichWants.sprite = tomatoSprite;
			thoughtBubble.SetActive(true);
			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case lettuce:
			sandwichWants.sprite = lettuceSprite;
			thoughtBubble.SetActive(true);

			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case cheese:
			sandwichWants.sprite = cheeseSprite;
			thoughtBubble.SetActive(true);

			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case ham:
			sandwichWants.sprite = hamSprite;
			thoughtBubble.SetActive(true);
		
			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case chicken:
			sandwichWants.sprite = chickenSprite;
			thoughtBubble.SetActive(true);

			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case onion:
			sandwichWants.sprite = onionSprite;
			thoughtBubble.SetActive(true);

			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case toasty:
			sandwichWants.sprite = toastingSprite;
			thoughtBubble.SetActive(true);

			if(grillController.onGrill)
			{
				sandwichState = 0;
			}
			break;
		case moldy:
			sandwichWants.sprite = moldySprite;
			thoughtBubble.SetActive(true);
			
			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case dogfood:
			sandwichWants.sprite = dogFoodSprite;
			thoughtBubble.SetActive(true);
			
			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case glass:
			sandwichWants.sprite = glassSprite;
			thoughtBubble.SetActive(true);
			
			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case knife:
			sandwichWants.sprite = knifeSprite;
			thoughtBubble.SetActive(true);
			
			if(fed)
			{
				fed = false;
				sandwichState = 0;
				
			}
			break;
		case cutMe:
			sandwichWants.sprite = cuttingSprite;
			thoughtBubble.SetActive(true);
			willEat = false;


			break;
		}


			if (currentSize < maxSize) {
				if (playerController.thrownFood.Count != 0) {
					MoveSandwich ();
				}
			} else if (currentSize == maxSize) {
				sandwichState = cutMe;
				EndMovement ();
			}

	}

    void OnTriggerEnter(Collider other)
    {
		if (willEat && other.gameObject.tag == "Food") {
			FoodStats food = other.gameObject.GetComponent<FoodStats> ();
			if (food.edible)
			{
				Eat (other,food);
				StateComparison(other,food);
				SandwichRating(food);

				fed = true;
				currentSize++;
			}

		}
	}
			

	public void Eat(Collider other, FoodStats food)
	{
		Cursor.visible = true;
			
		if (food.edible && currentSize < maxSize) 
		{

			myAnimator.SetBool ("isMoving", false);
			myAnimator.SetBool ("foodTrigger", true);
			Destroy (other.gameObject.GetComponent<Rigidbody> ());
			foreach (BoxCollider bc in food.myColliders)
				bc.enabled = false;
			
			other.transform.parent = this.transform;
			sandwichTop.transform.position += new Vector3 (0f, food.ingredientHeight, 0f);
			other.transform.position = (sandwichTop.transform.position - new Vector3 (0f, (food.ingredientHeight), 0f));
			
			GetComponent<BoxCollider> ().center += new Vector3 (0f, (food.ingredientHeight / 2), 0f);
			GetComponent<BoxCollider> ().size += new Vector3 (0f, (food.ingredientHeight), 0f);
			
			cry.enabled = false;
			GameObject.Find ("AudioManager").SendMessage ("Play", "Eating1");
	
		}
	}

	public void StateComparison(Collider other, FoodStats food)
	{
		if (sandwichState == food.ingredientValue) {
			particleSystemRenderer.material = happyState;
			particleSystemEmotion.enableEmission = true;
			emissionStart = true;
			if (happyCounter >= 1) {
				GameObject.Find ("AudioManager").SendMessage ("Play", "Happy");
				happyCounter = 0;
			}              
			matching = true;
			cryingParticleRight.enableEmission = false;
			cryingParticleLeft.enableEmission = false;
			cryLimit = 0;
			happyCounter++;
		} else {
			particleSystemRenderer.material = sadState;
			particleSystemEmotion.enableEmission = true;
			emissionStart = true;
			matching = false;
			cryLimit++;

		}
	}

	public void SandwichRating(FoodStats food)
	{
		if (food.goodIngredient) {
			rating++;
		} else if (!food.goodIngredient) {
			if (rating > 0) {
				rating--;
			}
		}
		recipe.AddIngredient (food.ingredientType.ToString (), matching);
		comments.AddComment (food.goodIngredient);
		if (rating < 10) {
			comments.RateSandwich (rating);
		}
	}



	void checkEmission ()
	{
		emissionTimer -= Time.deltaTime;
		if (emissionTimer <= 0) 
		{
			particleSystemEmotion.enableEmission = false;
			emissionTimer = emissionTimerReset;
			emissionStart = false;
		}
	}


	public void SandwichBehaviour () {
		wantTimer += Time.deltaTime;
		if (wantTimer >= 3.0f) {
			if(currentSize < 5)
			{
				sandwichState = Random.Range (1, 8);
				wantTimer = 0.0f;
			}
			else if (currentSize < 10)
			{
				sandwichState = Random.Range (1, 11);
				wantTimer = 0.0f;
			}
			else if(currentSize < 15)
			{
				sandwichState = Random.Range (8, 12);
				wantTimer = 0.0f;
			}

		}
	}

    public void MoveSandwich()
    {
        if (nextFood >= playerController.thrownFood.Count)
            return;

            targetFood = playerController.thrownFood[nextFood].transform.position;

            targetFood.y = transform.position.y;

            Vector3 moveDirection = targetFood - transform.position;

		    myAnimator.SetBool("isMoving", true);
		    //anim.Play ("Bobbing", PlayMode.StopAll);


        if (moveDirection.magnitude <= 0.5)
        {
                nextFood++;
        }
        else
        {
            sandwichNav.SetDestination(targetFood);
        }
    }


	public void EndMovement()
	{
		sandwichNav.SetDestination (endTarget.transform.position);
	}


    public AudioSource cry;

	void CryCheck()
	{
		if (cryLimit >= cryLimitSet) 
		{
			cryingParticleRight.enableEmission = true;
			cryingParticleLeft.enableEmission = true;
            Debug.Log("Cry");
            cry.enabled = true;
            //anim.Play ("sadv2");
        }
	}

}
