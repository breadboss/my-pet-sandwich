﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Recipe : MonoBehaviour {

	public CustomizerController nameControl;
	public Text sandwichName;
	public Sandwich sandwich;
	public Image ingredientImage;
	public Text ingredientText;
	public Canvas recipe;
	private int ingredientNum = 0;

	
	// Use this for initialization
	void Start () {
		
		sandwich = GameObject.Find ("Sandwich").GetComponent<Sandwich>();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (nameControl.isNamed) {
			sandwichName.text = PlayerPrefs.GetString ("SandwichName");
		}

	
	}

	public void AddIngredient(string ingredient, bool matching)
	{
		Vector3 pos = new Vector3 (-150, 121 -(24 * ingredientNum), 0);
		Text tempIngredientText = Instantiate (ingredientText, pos, Quaternion.identity) as Text;
		tempIngredientText.text = ingredient;
		if (matching) {
			tempIngredientText.color = Color.green;
		} else {
			tempIngredientText.color = Color.red;
		}
		tempIngredientText.transform.SetParent (recipe.transform, false);
		ingredientNum++;
		
	}
}
