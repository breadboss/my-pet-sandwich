﻿using UnityEngine;
using System.Collections;

public class FoodStats : MonoBehaviour {
	public float ingredientHeight;

    public Sandwich sandwich;//Get the sandwich script
	public bool otherAngle;//To not change the angle of the original form
	public bool edible;
	public bool goodIngredient;
	private float edibleTimer = 5.0f;
	public bool dogTreatAngle;//To not change the angle of the original form
	public bool glassAngle;//To change the glass angle

    //Search for the sandwich script
    public void Awake()
    {
        sandwich = GameObject.Find("Sandwich").GetComponent<Sandwich>();
		edible = true;
    }

	public enum IngredientType {
		Idle,
		Tomato,
		Lettuce,
		Cheese,
		Ham,
		Chicken,
		Onion,
		Toasted,
		MoldyCheddar,
		DogTreat,
		BrokenGlass,
		Knife

	};

	public IngredientType ingredientType;
	public int ingredientValue;
	public BoxCollider[] myColliders;

	// Use this for initialization
	void Start () {
		myColliders = gameObject.GetComponents<BoxCollider> ();
		//transform.Rotate (90, 0, 0);
		ingredientHeight = GetComponent<Renderer> ().bounds.size.y;

		ingredientValue = (int)ingredientType;

	}
	// Update is called once per frame
	void Update () {
//		Debug.Log (ingredientHeight);
		if (glassAngle && !dogTreatAngle) {
			transform.localRotation = Quaternion.Euler (0, 0, 0);
		}

		if (dogTreatAngle && !glassAngle) {
			transform.localRotation = Quaternion.Euler (-90, 0, 90);
		}
		if (!dogTreatAngle && !glassAngle) {
			transform.localRotation = Quaternion.Euler (-90, 0, 0);
		}
		HeightCheck ();

		if (edibleTimer > 0) {
			edibleTimer -= Time.deltaTime;
			if (edibleTimer <= 0) {
				edible = false;
			}
		}

	}
	void HeightCheck()
	{
		if (transform.position.y <= -4)
		{
			Destroy(gameObject);
            sandwich.nextFood++;//Next objects will become the next food
		}
	}
}
	